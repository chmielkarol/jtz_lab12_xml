<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <html>
            <body>
                <h1>Specjalnie dla Ciebie!!!</h1>
                <div>

                </div>
                    <a> Biuro podróży:
                        <xsl:value-of select="offer/agency"/>
                    </a>
                <div>
                    <a style="color:red;"> Opis:
                    </a>
                    <a>
						<xsl:value-of select="offer/desc"/>
                    </a>
                </div>
                <div>
                    <a>
                     Numer oferty: <xsl:value-of select="offer/number"/>
                    </a>
                </div>
                <div>
                    <a>
                      Data:  <xsl:value-of select="offer/date"/>
                    </a>
                </div>
                <div>
                    <a style="color:red;"> Cena:
                        <xsl:value-of select="offer/price"/>
                    </a>
                </div>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>