<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <html>
            <body>
                <table style="height: 426px; width: 240px; margin-left: auto; margin-right: auto;">
					<tbody>
						<tr>
							<td style="width: 240px; background-color: #050505; text-align: center;" colspan="2"><span style="color: #ffffff;"><strong>OFERTA NR <span style="color: #ff0000;"><xsl:value-of select="offer/number"/></span></strong></span></td>
						</tr>
						<tr>
							<td style="background-color: #d0d0d0;"><strong>Biuro podróży</strong></td>
							<td style="width: 158px; text-align: center;"><xsl:value-of select="offer/agency"/></td>
						</tr>
						<tr>
							<td style="background-color: #d0d0d0;"><strong>Termin</strong></td>
							<td style="width: 158px; text-align: center;"><xsl:value-of select="offer/date"/></td>
						</tr>
						<tr>
							<td style="background-color: #d0d0d0;"><strong>Cena</strong></td>
							<td style="width: 158px; text-align: center;"><xsl:value-of select="offer/price"/></td>
						</tr>
						<tr>
							<td style="background-color: #d0d0d0;"><strong>Opis</strong></td>
							<td style="width: 158px; text-align: center;"><xsl:value-of select="offer/desc"/></td>
						</tr>
					</tbody>
				</table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>