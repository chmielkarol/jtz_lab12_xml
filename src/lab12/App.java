package lab12;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.swing.UIManager;
import java.awt.Color;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

public class App {

	private JFrame frmBiuroPodrzy;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	
	private ObjectFactory factory;
	
	private DefaultListModel<String> model;
	private DefaultListModel<String> model_1;
	private JTextPane textPane;
	private JList<String> list;
	private JList<String> list_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					App window = new App();
					window.frmBiuroPodrzy.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public App() {
		factory = new ObjectFactory();
		model = new DefaultListModel<>();
		model_1 = new DefaultListModel<>();
		
		initialize();
		
		update();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmBiuroPodrzy = new JFrame();
		frmBiuroPodrzy.setTitle("Biuro podr\u00F3\u017Cy");
		frmBiuroPodrzy.setBounds(100, 100, 650, 451);
		frmBiuroPodrzy.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frmBiuroPodrzy.getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Edycja ofert", null, panel, null);
		panel.setLayout(null);
		
		JPanel panel_7 = new JPanel();
		panel_7.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Oferta", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_7.setBounds(380, 28, 138, 275);
		panel.add(panel_7);
		panel_7.setLayout(null);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(6, 16, 125, 43);
		panel_7.add(panel_2);
		panel_2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Numer", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_2.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(6, 16, 113, 20);
		panel_2.add(textField);
		textField.setColumns(10);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBounds(7, 58, 125, 43);
		panel_7.add(panel_3);
		panel_3.setLayout(null);
		panel_3.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Biuro", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(6, 16, 113, 20);
		panel_3.add(textField_1);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBounds(7, 100, 125, 43);
		panel_7.add(panel_4);
		panel_4.setLayout(null);
		panel_4.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Termin", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(6, 16, 113, 20);
		panel_4.add(textField_2);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBounds(7, 144, 125, 43);
		panel_7.add(panel_5);
		panel_5.setLayout(null);
		panel_5.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Cena", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(6, 16, 113, 20);
		panel_5.add(textField_3);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBounds(7, 186, 125, 43);
		panel_7.add(panel_6);
		panel_6.setLayout(null);
		panel_6.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Opis", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(6, 16, 113, 20);
		panel_6.add(textField_4);
		
		JButton btnNewButton = new JButton("Zapisz\r\n");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Offer obj = factory.createOffer();
				obj.number = Integer.parseInt(textField.getText());
				obj.agency = textField_1.getText();
				obj.date = textField_2.getText();
				obj.price = Integer.parseInt(textField_3.getText());
				obj.desc = textField_4.getText();
				
				try {
					JAXBContext context = JAXBContext.newInstance(Offer.class);
					
					Marshaller marshaller = context.createMarshaller();
					
					marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
					
					File file = new File("./dane/" + textField.getText() + ".xml");
					
					marshaller.marshal(obj, file);
				}
				catch (JAXBException e) {
					e.printStackTrace();
				}
				
			}
		});
		btnNewButton.setBounds(27, 240, 89, 23);
		panel_7.add(btnNewButton);
		
		JPanel panel_10 = new JPanel();
		panel_10.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Oferty", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_10.setBounds(52, 30, 274, 272);
		panel.add(panel_10);
		panel_10.setLayout(null);
		
		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(6, 16, 262, 249);
		panel_10.add(scrollPane_3);
		
		JList<String> list_2 = new JList<>();
		list_2.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				if(list_2.getSelectedIndex() != -1) {
			        File file = new File("./dane/" + list_2.getSelectedValue());
			        try {
				        JAXBContext jaxbContext = JAXBContext.newInstance(Offer.class);
				        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
						Offer offer = (Offer) unmarshaller.unmarshal(file);
						textField.setText(String.valueOf(offer.getNumber()));
						textField_1.setText(offer.getAgency());
						textField_2.setText(offer.getDate());
						textField_3.setText(String.valueOf(offer.getPrice()));
						textField_4.setText(offer.getDesc());
					} catch (JAXBException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		scrollPane_3.setViewportView(list_2);
		list_2.setModel(model);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Wy\u015Bwietlanie ofert", null, panel_1, null);
		panel_1.setLayout(null);
		
		JPanel panel_8 = new JPanel();
		panel_8.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Oferty", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_8.setBounds(16, 66, 110, 242);
		panel_1.add(panel_8);
		panel_8.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 16, 98, 219);
		panel_8.add(scrollPane);
		
		ListSelectionListener lsl = new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				if(list.getSelectedIndex() != -1) {
					if(list_1.getSelectedIndex() != -1) {
						StringWriter sw = new StringWriter();
						Source xml = new StreamSource(new File("./dane/" + list.getSelectedValue()));
						Source xslt = new StreamSource("./transformacje/" + list_1.getSelectedValue());
						try {
						    FileWriter fw = new FileWriter("out.html");
						    TransformerFactory tFactory = TransformerFactory.newInstance();
						    Transformer trasform = tFactory.newTransformer(xslt);
						    trasform.transform(xml, new StreamResult(sw));
						    fw.write(sw.toString());
						    fw.close();
						
						} catch (IOException | TransformerException | TransformerFactoryConfigurationError e) {
						    e.printStackTrace();
						}
						
						try {
						    textPane.setText(new String(Files.readAllBytes(Paths.get("out.html"))));
						} catch (IOException e) {
						    e.printStackTrace();
						}
					}
				}
			}
		};
		
		list = new JList<>();
		list.addListSelectionListener(lsl);
		list.setModel(model);
		scrollPane.setViewportView(list);
		
		JLabel lblNewLabel = new JLabel(">");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblNewLabel.setBounds(129, 163, 27, 29);
		panel_1.add(lblNewLabel);
		
		JPanel panel_9 = new JPanel();
		panel_9.setLayout(null);
		panel_9.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Szablony styli", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_9.setBounds(151, 66, 110, 242);
		panel_1.add(panel_9);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(6, 16, 98, 219);
		panel_9.add(scrollPane_1);

		list_1 = new JList<>();
		list_1.addListSelectionListener(lsl);
		list_1.setModel(model_1);
		scrollPane_1.setViewportView(list_1);
		
		JLabel label = new JLabel(">");
		label.setFont(new Font("Tahoma", Font.PLAIN, 30));
		label.setBounds(259, 163, 27, 29);
		panel_1.add(label);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(292, 16, 327, 359);
		panel_1.add(scrollPane_2);
		
		textPane = new JTextPane();
		textPane.setContentType("text/html");
		scrollPane_2.setViewportView(textPane);
	}
	
	private void update() {
		Thread thread = new Thread(new Runnable() {
			public void run() {
				while(true) {
					try {
						Thread.sleep(500);
						File f = new File("./dane");
						File[] files = f.listFiles(new FilenameFilter() {
							public boolean accept(File dir, String name) {
								return name.endsWith("xml");
							}
						});
						if(files.length != model.getSize()) {
							model.clear();
							for (File file : files) {
								model.addElement(file.getName());
							}
						}

						f = new File("./transformacje");
						files = f.listFiles(new FilenameFilter() {
							public boolean accept(File dir, String name) {
								return name.endsWith("xsl");
							}
						});
						if(files.length != model_1.getSize()) {
							model_1.clear();
							for (File file : files) {
								model_1.addElement(file.getName());
							}
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		thread.start();
	}
}
